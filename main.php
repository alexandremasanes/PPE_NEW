<?php

use Doctrine\ORM\Query\QueryException;

require_once 'App/Library/vendor/autoload.php';
require_once 'App/autoload.php';
require_once 'App/config.php';

if(strpos(URL,'main.php') !== FALSE || strpos(URL,'?')){
	echo new ErrorView(Response::NOT_FOUND);
	exit;
}

try{
	extract($_GET);
	unset($_GET);
    Dispatcher::dispatch($url??'');
	echo Dispatcher::deliver();
	Dispatcher::close();

}catch(PDOException | QueryException $t){
    Session::save();
    Log::write($t->getMessage().PHP_EOL.$t->getTraceAsString());

} catch(Throwable $t) {
    Session::save();
    if (!PROD)
        throw $t;

    else if ($t instanceof TypeError && strpos($t->getTrace()[0]['file'], 'Dispatcher.php'))
        echo new ErrorView(Response::SERVICE_UNAVAILABLE);

    else if($t instanceof MethodNotAllowedException)
        echo new ErrorView(Response::METHOD_NOT_ALLOWED);

    else if($t instanceof BadRequestException)
        echo new ErrorView(Response::BAD_REQUEST);

    else if ($t instanceof NotFoundException)
        echo new ErrorView(Response::NOT_FOUND);

    else
        echo new ErrorView(Response::SERVICE_UNAVAILABLE);
}