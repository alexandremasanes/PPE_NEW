let insurances_select = $(" #insurances ");
let amount_select = $(" #amount ");
let deductible_select = $(" #deductible ");
let default_amount_select = $(" #default_amount ");

$().ready(run);

function run(){
    console.log(">>>run>>>");
    let insurances = null;
    let selected_insurance = null;
    let clicked = false;
    insurances_select.click(function(){
        if(!clicked){
            retrieve_insurances();
            clicked = true;
        }
    });

    console.log("<<<run<<<")
}

function display_deductible(amnt, insurance) {
    console.log(insurance);
    min = insurance.min_deductible;
    max = insurance.max_deductible;
    console.log(min+" "+max);
    let val;
    if (amnt == 0)
        val= 0;

    //else if ((amnt * 10 / 100) <= min && obj.code == '')
      //  val=(0 + ' €');

    else if ((amnt * 10 / 100) <= min)
        val= min;

    else if ((amnt * 10 / 100) >= max)
        val= max;

    else
        val= amnt * 10 / 100;

    deductible_select.val(val);
}

function retrieve_insurances(){
    console.log(">>>retrieve_insurances>>>");

    $.ajax({
        url: BASE_URL+'user-api/get-insurances/',
        type: 'GET',
        dataType: 'json',
        complete: function(result, status) {
           if (!result)
               return;
            console.log(result.responseText);
            insurances = JSON.parse(result.responseText);
            bind_events();
        }
    });

    console.log("<<<retrieve_insurances<<<",insurances);
}

function bind_events(){
    $.each(insurances, function (i, insurance) {
        let o = new Option(insurance.title, insurance.id);
        $(o).html(insurance.title);
        insurances_select.append(o);
    });
    insurances_select.change(function(){
        amount_select.val(null);
        deductible_select.val(null);
        default_amount_select.val(null);
        let index = insurances_select.val()-1;
        if(index.toString().trim()) {
            selected_insurance = insurances[index];
            console.log(selected_insurance.id);
            default_amount_select.val(selected_insurance.default_amount);
        }
    });
    amount_select.keyup(function(){
        let amount = amount_select.val();
        console.log(selected_insurance, amount);
        if(selected_insurance && amount) {
            display_deductible(amount, selected_insurance);
        }
    });
}