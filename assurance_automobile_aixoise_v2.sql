DROP DATABASE IF EXISTS aaa;

CREATE DATABASE aaa;
USE aaa;

# -----------------------------------------------------------------------------
#       TABLE : statuts
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS statuts(
  id INTEGER NOT NULL AUTO_INCREMENT,
  description VARCHAR(100),
  maj TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY(ID)
);

# -----------------------------------------------------------------------------
#       TABLE : marques
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS marques(
  id INTEGER NOT NULL AUTO_INCREMENT,
  nom VARCHAR(50) NULL,
  maj TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (ID) 
 );

# -----------------------------------------------------------------------------
#       TABLE : modeles
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS modeles(
  id INTEGER NOT NULL AUTO_INCREMENT,
  nom VARCHAR(50) NULL  ,
  annee BIGINT(4) NULL,
  maj TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  marque_id INTEGER,
  PRIMARY KEY (id),
  FOREIGN KEY(marque_id) REFERENCES marques(id) ON DELETE CASCADE

 );

# -----------------------------------------------------------------------------
#       TABLE : vehicules
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS vehicules(
  id INTEGER NOT NULL AUTO_INCREMENT,
  vin CHAR(17) NOT NULL UNIQUE,
  modele_id INTEGER NOT NULL,
  maj TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY(modele_id) REFERENCES modeles(id) ON DELETE CASCADE
 );


# -----------------------------------------------------------------------------
#       TABLES : PERSONNES, ASSURES, UTILISATEURS
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS personnes(
  ID INTEGER PRIMARY KEY  AUTO_INCREMENT,
  NOM VARCHAR(25) NULL,
  PRENOM VARCHAR(25) NULL,
  ADRESSE VARCHAR(25) NULL,
  VILLE VARCHAR(25) NULL,
  CODE_POSTAL BIGINT(5) NULL,
  TEL VARCHAR(13) NULL,
  MAJ TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  TYPE TINYINT
 );

CREATE TABLE IF NOT EXISTS ASSURES(
  ID INTEGER PRIMARY KEY,
  NOM VARCHAR(25) NULL,
  PRENOM VARCHAR(25) NULL,
  ADRESSE VARCHAR(25) NULL,
  VILLE VARCHAR(25) NULL,
  CODE_POSTAL BIGINT(5) NULL,
  TEL VARCHAR(13) NULL,
  BONUS_MALUS REAL(5,2) DEFAULT 1.0,
  MAJ TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (ID) REFERENCES PERSONNES(ID) ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS TIERS(
  ID INTEGER PRIMARY KEY,
  NOM VARCHAR(25) NULL,
  PRENOM VARCHAR(25) NULL,
  ADRESSE VARCHAR(25) NULL,
  VILLE VARCHAR(25) NULL,
  CODE_POSTAL BIGINT(5) NULL,
  TEL VARCHAR(13) NULL,
  BONUS_MALUS REAL(5,2) DEFAULT 1.0,
  COMPAGNIE VARCHAR(128) NULL,
  MAJ TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (ID) REFERENCES ASSURES(ID) ON DELETE CASCADE
 );


CREATE TABLE IF NOT EXISTS EXPERTS(
  ID INTEGER PRIMARY KEY,
  NOM VARCHAR(25) NULL,
  PRENOM VARCHAR(25) NULL,
  ADRESSE VARCHAR(25) NULL,
  VILLE VARCHAR(25) NULL,
  CODE_POSTAL BIGINT(5) NULL,
  TEL VARCHAR(13) NULL,
  RANG BIGINT(4) NULL,
  MAJ TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (ID) REFERENCES PERSONNES(ID) ON DELETE CASCADE
)comment = "";

 CREATE TABLE IF NOT EXISTS CLIENTS(
  ID INTEGER PRIMARY KEY,
  NOM VARCHAR(25) NULL,
  PRENOM VARCHAR(25) NULL,
  ADRESSE VARCHAR(25) NULL,
  VILLE VARCHAR(25) NULL,
  CODE_POSTAL BIGINT(5) NULL,
  TEL VARCHAR(13) NULL,
  BONUS_MALUS REAL(5,2) DEFAULT 1.0,
  CARTE_IDENTITE VARCHAR(100),
  SEPA VARCHAR(100),
  MAJ TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY(ID) REFERENCES ASSURES(ID) ON DELETE CASCADE
);

 CREATE TABLE IF NOT EXISTS ADMINISTRATEURS(
   ID INTEGER PRIMARY KEY,
   NOM VARCHAR(25) NULL,
   PRENOM VARCHAR(25) NULL,
   ADRESSE VARCHAR(25) NULL,
   VILLE VARCHAR(25) NULL,
   CODE_POSTAL BIGINT(5) NULL,
   TEL VARCHAR(13) NULL,
   ROLE VARCHAR(128) NULL,
   MAJ TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   FOREIGN KEY (ID) REFERENCES PERSONNES(ID) ON DELETE CASCADE
 );

# -----------------------------------------------------------------------------
#       TABLE : COMPTES_UTILISATEUR
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS COMPTES_UTILISATEUR(
  ID INTEGER PRIMARY KEY,
  EMAIL VARCHAR(50) NOT NULL,
  HASH CHAR(40) NULL,
  TOKEN char(8) NULL,
  MAJ TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY(ID) REFERENCES PERSONNES(ID) ON DELETE CASCADE
);

# -----------------------------------------------------------------------------
#       TABLE : INDEMNISATIONS
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS INDEMNISATIONS(
   ID INTEGER AUTO_INCREMENT PRIMARY KEY,
   ASSURE_ID INTEGER NOT NULL,
   MONTANT FLOAT(10,2) NULL,
   FRANCHISE FLOAT(10,2) NULL,
   MAJ TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   FOREIGN KEY(CLIENT_ID) REFERENCES CLIENTS(ID) ON DELETE CASCADE
);

# -----------------------------------------------------------------------------
#       TABLE : garagistes
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS garagistes(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  nom VARCHAR(128) NULL,
  agree BOOLEAN DEFAULT 0,
  maj TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
 );

# -----------------------------------------------------------------------------
#       TABLE : types_sinistres_sans_tiers
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS types_sinistres_sans_tiers(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  code ENUM('INC','VOL','BRI') UNIQUE,
  libelle VARCHAR(50),
  maj TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

# -----------------------------------------------------------------------------
#       TABLES : sinistres, accidents, sinistres_sans_tiers
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS sinistres(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  expert_id INTEGER,
  commentaire VARCHAR(100) NULL  ,
  date_s DATE NULL,
  heure TIME NULL,
  constat VARCHAR(100) NULL  ,
  maj TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  TYPE BOOLEAN,
  FOREIGN KEY (expert_id) REFERENCES experts(id) ON DELETE SET NULL
);


CREATE TABLE IF NOT EXISTS accidents(
  id INTEGER PRIMARY KEY,
  nb_pertes_h bigint(4) NULL,
  FOREIGN KEY (id) REFERENCES sinistres(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS sinistres_sans_tiers(
  id INTEGER PRIMARY KEY,
  type_id INTEGER NOT NULL,
  maj TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY(id) REFERENCES sinistres(id) ON DELETE CASCADE,
  FOREIGN KEY(type_id) REFERENCES types_sinistres_sans_tiers(id) ON DELETE CASCADE
);

# -----------------------------------------------------------------------------
#       TABLE : types_garantie
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS types_garantie(
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  code enum('tie','inc','vol','tou') UNIQUE,
  libelle VARCHAR(100) NULL,
  montant_defaut FLOAT(13,2) NULL,
  franchise_min FLOAT(13,2) NULL,
  franchise_max FLOAT(13,2) NULL,
  maj TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

# -----------------------------------------------------------------------------
#       TABLES : vehicules_sinistres
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS vehicules_sinistres(
  vehicule_id INTEGER,
  sinistre_id INTEGER,
  PRIMARY KEY(vehicule_id, sinistre_id),
  FOREIGN KEY(vehicule_id) REFERENCES vehicules(ID),
  FOREIGN KEY(sinistre_id) REFERENCES sinistres(ID)
);

# -----------------------------------------------------------------------------
#       TABLES : DOMMAGES, DETERIORATION, DESTRUCTIONS
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS dommages(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  sinistre_id INTEGER NOT NULL,
  vehicule_id INTEGER NOT NULL,
  description VARCHAR(100) NULL,
  montant FLOAT(12,2) NULL,
  maj TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  type BOOLEAN,
  UNIQUE(sinistre_id, vehicule_id),
  FOREIGN KEY(sinistre_id, vehicule_id) REFERENCES vehicules_sinistres(sinistre_id, vehicule_id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS deteriorations(
  id INTEGER PRIMARY KEY,
  garagiste_id INTEGER,
  taux DECIMAL(10,2),
  FOREIGN KEY(id) REFERENCES dommages(id) ON DELETE CASCADE,
  FOREIGN KEY(garagiste_id) REFERENCES garagistes(id) ON DELETE SET NULL
)comment = "";


CREATE TABLE IF NOT EXISTS destructions(
  id INTEGER PRIMARY KEY,
  est_total BOOLEAN,
  FOREIGN KEY(id) REFERENCES dommages(id) ON DELETE CASCADE
);

# -----------------------------------------------------------------------------
#       TABLE : immatriculations
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS immatriculations(
  id INTEGER PRIMARY KEY,
  code CHAR(9),
  assure_id INTEGER NOT NULL,
  carte_grise VARCHAR(100),
  date_achat DATE,
  type BIT,
  maj TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY(id) REFERENCES vehicules(id) ON DELETE CASCADE,
  FOREIGN KEY(assure_id) REFERENCES assures(id) ON DELETE CASCADE
);

# -----------------------------------------------------------------------------
#       TABLE : h_immatriculations
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS h_immatriculations(
  id INTEGER,
  code CHAR(9),
  assure_id INTEGER NOT NULL,
  carte_grise VARCHAR(100),
  date_achat DATE,
  type BOOLEAN,
  maj TIMESTAMP,
  date_h TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id, date_h),
  FOREIGN KEY(assure_id) REFERENCES assures(id) ON DELETE CASCADE,
  FOREIGN KEY(id) REFERENCES vehicules(id) ON DELETE CASCADE
);


# -----------------------------------------------------------------------------
#       TABLE : contrats
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS contrats(
   id INTEGER PRIMARY KEY AUTO_INCREMENT,
   immat_id INTEGER NOT NULL UNIQUE,
   type_garantie_id INTEGER NOT NULL,
   montant REAL(5, 2) NULL,
   date_souscription DATE NULL,
   etat BIGINT(4) NULL ,
   contrat VARCHAR(100) NULL,
   maj TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   FOREIGN KEY(immat_id) REFERENCES immatriculations(id) ON DELETE CASCADE,
   FOREIGN KEY(type_garantie_id) REFERENCES types_garantie(id) ON DELETE CASCADE
);

# -----------------------------------------------------------------------------
#       TABLE : h_contrats
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS h_contrats(
   id INTEGER NOT NULL,
   immat_id INTEGER NOT NULL,
   immat_date_h INTEGER NOT NULL,
   type_garantie_id INTEGER NOT NULL,
   montant REAL(5, 2) NULL,
   date_souscription DATE NULL,
   etat BIGINT(4) NULL ,
   contrat VARCHAR(100) NULL,
   maj TIMESTAMP,
   date_h TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY(id, date_h),
   UNIQUE(immat_id, immat_date_h),
   FOREIGN KEY(immat_id, immat_date_h) REFERENCES h_immatriculations(id, date_h) ON DELETE CASCADE,
   FOREIGN KEY(type_garantie_id) REFERENCES types_garantie(id) ON DELETE CASCADE
);

# -----------------------------------------------------------------------------
#       TABLE : vehicules_accidents
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS vehicules_accidents(
  vehicule_id INTEGER NOT NULL,
  accident_id INTEGER NOT NULL,
  taux_resp REAL(5,2) NULL,
  maj TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (vehicule_id, accident_id),
  FOREIGN KEY(vehicule_id) REFERENCES vehicules(id) ON DELETE CASCADE ,
  FOREIGN KEY(accident_id) REFERENCES accidents(id) ON DELETE CASCADE
);

# -----------------------------------------------------------------------------
#       TABLE : types_sinistres_types_garantie
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS types_sinistres_types_garantie(
  type_sinistres_id INTEGER NOT NULL,
  type_garantie_id INTEGER NOT NULL,
  maj TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (type_sinistres_id, type_garantie_id),
  FOREIGN KEY(type_sinistres_id) REFERENCES types_sinistres_sans_tiers(id) ON DELETE CASCADE,
  FOREIGN KEY(type_garantie_id) REFERENCES types_garantie(id) ON DELETE CASCADE
);


# -----------------------------------------------------------------------------
#       TABLE : affectations
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS affectations(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  admin_id INTEGER NOT NULL,
  statut_id INTEGER NOT NULL,
  client_id INTEGER,
  contrat_id INTEGER,
  sinistre_id INTEGER,
  notif VARCHAR(50),
  recu BOOLEAN DEFAULT 0,
  maj TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  type TINYINT,
  FOREIGN KEY(admin_id) REFERENCES administrateurs(id) ON DELETE CASCADE,
  FOREIGN KEY(statut_id) REFERENCES statuts(id) ON DELETE CASCADE,
  FOREIGN KEY(client_id) REFERENCES clients(id) ON DELETE CASCADE,
  FOREIGN KEY(contrat_id) REFERENCES contrats(id) ON DELETE CASCADE,
  FOREIGN KEY(sinistre_id) REFERENCES sinistres(id) ON DELETE CASCADE
);
/**
# -----------------------------------------------------------------------------
#       TABLE : PERSONNE_COMPTE_UTILISATEUR
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS PERSONNES_COMPTES_UTILISATEUR(
  PERSONNE_ID INTEGER NOT NULL UNIQUE,
  COMPTE_UTILISATEUR_ID INTEGER NOT NULL UNIQUE,
  FOREIGN KEY(PERSONNE_ID) REFERENCES PERSONNES(ID) ON DELETE CASCADE,
  FOREIGN KEY(COMPTE_UTILISATEUR_ID) REFERENCES COMPTES_UTILISATEUR(ID) ON DELETE CASCADE
);**/

# -----------------------------------------------------------------------------
#       INSERT : marques
# -----------------------------------------------------------------------------

INSERT INTO
  marques(nom)
SELECT DISTINCT make FROM vehicles_sources.vehicle_model_year;

# -----------------------------------------------------------------------------
#       INSERT : modeles
# -----------------------------------------------------------------------------

INSERT INTO 
  modeles(nom, annee, marque_id)
SELECT vsvmy.Model, Year, m.ID
FROM Vehicles_sources.Vehicle_Model_Year vsvmy, Marques m
WHERE vsvmy.make = m.NOM;

# -----------------------------------------------------------------------------
#       INSERT : types_garantie
# -----------------------------------------------------------------------------

INSERT INTO 
  types_garantie
VALUES
  (NULL,1,'Tiers',300,300,500, NULL),
  (NULL,2,'Incendie',600,200,400, NULL),
  (NULL,3,'Vol',600,200,400, NULL),
  (NULL,4,'Tout Risque',1000,0,100, NULL);

# -----------------------------------------------------------------------------
#       INSERT : types_sinistres_sans_tiers
# -----------------------------------------------------------------------------

INSERT INTO
  types_sinistres_sans_tiers
VALUES
  (NULL,1,'Incendie'),
  (NULL,2,'Vol'),
  (NULL,3,'Bris de glace');

# -----------------------------------------------------------------------------
#       INSERT : statuts
# -----------------------------------------------------------------------------

INSERT INTO
  statuts
VALUES
  (NULL,'En cours de validation'),
  (NULL,'Valide'),
  (NULL,'invalide');

# -----------------------------------------------------------------------------
#       FUNCTION : is_in_assures
# -----------------------------------------------------------------------------

DROP FUNCTION IF EXISTS is_in_assures;

DELIMITER $

CREATE FUNCTION is_in_assures(in_id INTEGER) RETURNS BIT
BEGIN
  DECLARE is_in_assures BIT DEFAULT 0;
  SELECT IF(COUNT(*) > 0, 1, 0)
  INTO is_in_assures
  FROM assures
  WHERE id = in_id;
  RETURN is_in_assures;
END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       PROCEDURE : check_if_in_assures
# -----------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS check_if_in_assures;

DELIMITER $

CREATE PROCEDURE check_if_in_assures(IN in_id INTEGER)
BEGIN
  IF NOT is_in_assures(in_id) THEN
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'Cannot insert into `clients` or `tiers`, primary key not found in `assures`';
  END IF;
END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       PROCEDURE : check_if_not_in_assures
# -----------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS check_if_not_in_assures;

DELIMITER $

CREATE PROCEDURE check_if_not_in_assures(IN in_id INTEGER)
BEGIN
  IF(is_in_assures(in_id)) THEN
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'Cannot insert into `admins` or `experts`, primary key found in `assures`';
  END IF;
END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       PROCEDURE : check_if_not_in_tiers
# -----------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS check_if_not_in_tiers;

DELIMITER $

CREATE PROCEDURE check_if_not_in_tiers(IN in_id INTEGER)
BEGIN
  DECLARE is_third_party BIT DEFAULT 0;
  SELECT IF(COUNT(*) > 0, 1, 0)
  INTO is_third_party
  FROM tiers
  WHERE id = in_id;

  IF(is_third_party) THEN
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'Cannot insert into `clients`, primary key found in `tiers`';
  END IF;
END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       PROCEDURE : check_if_not_in_clients
# -----------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS check_if_not_in_clients;

DELIMITER $

CREATE PROCEDURE check_if_not_in_clients(IN in_id INTEGER)
BEGIN
  DECLARE is_customer BIT DEFAULT 0;
  SELECT IF(COUNT(*) > 0, 1, 0)
  INTO is_customer
  FROM client
  WHERE id = in_id;

  IF(is_customer) THEN
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'Cannot insert into `tiers`, primary key found in `clients`';
  END IF;
END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       PROCEDURE : check_if_not_in_accidents
# -----------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS check_if_not_in_accidents;

DELIMITER $

CREATE PROCEDURE check_if_not_in_accidents(IN in_id INTEGER)
BEGIN
  DECLARE is_in_accidents BIT DEFAULT 0;
  SELECT IF(COUNT(*) > 0, 1, 0)
  INTO is_in_accidents
  FROM accidents
  WHERE id = in_id;

  IF(is_in_accidents) THEN
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'Cannot insert into `sinistres_sans_tiers`, primary key found in `accidents';
  END IF;
END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       PROCEDURE : check_if_not_in_sinistres_sans_tiers
# -----------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS check_if_not_in_sinistres_sans_tiers;

DELIMITER $

CREATE PROCEDURE check_if_not_in_sinistres_sans_tiers(IN in_id INTEGER)
BEGIN
  DECLARE is_in_sinistres_sans_tiers BIT DEFAULT 0;
  SELECT IF(COUNT(*) > 0, 1, 0)
  INTO is_in_sinistres_sans_tiers
  FROM sinistres_sans_tiers
  WHERE id = in_id;

  IF(is_in_sinistres_sans_tiers) THEN
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'Cannot insert into `accidents`, primary key found in `sinistre_sans_tiers';
  END IF;
END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       PROCEDURE : check_if_not_in_destructions
# -----------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS check_if_not_in_destructions;

DELIMITER $

CREATE PROCEDURE check_if_not_in_destructions(IN in_id INTEGER)
BEGIN
  DECLARE is_in_destructions BIT DEFAULT 0;
  SELECT IF(COUNT(*) > 0, 1, 0)
  INTO is_in_destructions
  FROM destructions
  WHERE id = in_id;

  IF(is_in_destructions) THEN
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'Cannot insert into `deteriorations`, primary key found in `destructions';
  END IF;
END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       PROCEDURE : check_if_not_in_deteriorations
# -----------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS check_if_not_in_deteriorations;

DELIMITER $

CREATE PROCEDURE check_if_not_in_deteriorations(IN in_id INTEGER)
BEGIN
  DECLARE is_in_deteriorations BIT DEFAULT 0;
  SELECT IF(COUNT(*) > 0, 1, 0)
  INTO is_in_deteriorations
  FROM deteriorations
  WHERE id = in_id;

  IF(is_in_deteriorations) THEN
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'Cannot insert into `destructions`, primary key found in `deteriorations';
  END IF;
END $

DELIMITER ;


# -----------------------------------------------------------------------------
#       TRIGGER : auto_affectation
# -----------------------------------------------------------------------------


DROP TRIGGER IF EXISTS auto_affectation;
DELIMITER $
CREATE TRIGGER auto_affectation 
AFTER INSERT ON clients
FOR EACH ROW
BEGIN
  DECLARE auto_admin_id INTEGER ;
  
  SELECT MIN(administrateurs.id)
  FROM administrateurs
  INTO auto_admin_id ;

  INSERT INTO 
    affectations(admin_id, client_id, statut_id)
  VALUES
    (auto_admin_id, NEW.id ,1) ;
END $
DELIMITER ;

# -----------------------------------------------------------------------------
#       TRIGGER : on_create_admin
# -----------------------------------------------------------------------------

DROP TRIGGER IF EXISTS on_create_admin;

DELIMITER $

CREATE TRIGGER on_create_admin
BEFORE INSERT ON administrateurs
FOR EACH ROW
BEGIN
  DECLARE is_expert BIT DEFAULT 0;

  SELECT IF(e.id IS NULL, 0, 1)
  INTO is_expert
  FROM personnes p LEFT JOIN experts e ON p.id = e.id
  WHERE p.id = NEW.id
  LIMIT 1;

  IF(is_in_assures(NEW.id)) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot insert into `administrateurs`, primary key found in `assures` !';
  END IF;


  IF(is_expert) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot insert into `administrateurs`, primary key found in `experts` !';
  END IF;

END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       TRIGGER : on_create_expert
# -----------------------------------------------------------------------------

DROP TRIGGER IF EXISTS on_create_expert;

DELIMITER $

CREATE TRIGGER on_create_expert
BEFORE INSERT ON experts
FOR EACH ROW
BEGIN
  DECLARE is_admin BIT DEFAULT 0;

  SELECT IF(adm.id IS NULL, 0, 1)
  INTO is_admin
  FROM personnes p LEFT JOIN admin adm  ON p.id = adm.id
  WHERE p.id = NEW.id
  LIMIT 1;

  IF(is_in_assures(NEW.id)) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot insert into `administrateurs`, primary key found in `assures` !';
  END IF;

  IF(is_admin) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot insert into `administrateurs`, primary key found in `experts` !';
  END IF;

END $

DELIMITER ;


# -----------------------------------------------------------------------------
#       TRIGGER : on_create_customer
# -----------------------------------------------------------------------------

DROP TRIGGER IF EXISTS on_create_customer;

DELIMITER $

CREATE TRIGGER on_create_customer
BEFORE INSERT ON clients
FOR EACH ROW
BEGIN

  DECLARE is_third_party BIT DEFAULT 0;

  CALL check_if_in_assures(NEW.id);

  SELECT IF(p.id IS NULL, 0, 1)
  INTO is_third_party
  FROM personnes p LEFT JOIN tiers t
  ON p.id = t.id
  WHERE p.id = NEW.id
  LIMIT 1;

  IF(is_third_party) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot insert into `clients`, primary key found in `tiers` !';
  END IF;

END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       TRIGGER : on_create_third_party
# -----------------------------------------------------------------------------

DROP TRIGGER IF EXISTS on_create_third_party;

DELIMITER $

CREATE TRIGGER on_create_third_party
BEFORE INSERT ON tiers
FOR EACH ROW
BEGIN
  DECLARE is_customer BIT DEFAULT 0;

  CALL check_if_in_assures(NEW.id);

  SELECT IF(p.id IS NULL, 0, 1)
  INTO is_customer
  FROM personnes p LEFT JOIN customer c
  ON p.id = c.id
  WHERE p.id = NEW.id
  LIMIT 1;

  IF(is_customer) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot insert into `tiers`, primary key found in `clients` !';
  END IF;

END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       TRIGGER : on_create_accident
# -----------------------------------------------------------------------------

DROP TRIGGER IF EXISTS on_create_accident;

DELIMITER $

CREATE TRIGGER on_create_accident
BEFORE INSERT ON accidents
FOR EACH ROW
BEGIN
  CALL check_if_not_in_sinistres_sans_tiers(NEW.id);
END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       TRIGGER : on_create_plain_sinister
# -----------------------------------------------------------------------------

DROP TRIGGER IF EXISTS on_create_plain_sinister;

DELIMITER $

CREATE TRIGGER on_create_plain_sinister
BEFORE INSERT ON sinistres_sans_tiers
FOR EACH ROW
BEGIN
  CALL check_if_not_in_accidents(NEW.id);
END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       TRIGGER : on_create_destruction
# -----------------------------------------------------------------------------

DROP TRIGGER IF EXISTS on_create_destruction;

DELIMITER $

CREATE TRIGGER on_create_destruction
BEFORE INSERT ON destructions
FOR EACH ROW
BEGIN
  CALL check_if_not_in_deteriorations(NEW.id);
END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       TRIGGER : on_create_deterioration
# -----------------------------------------------------------------------------

DROP TRIGGER IF EXISTS on_create_deterioration;

DELIMITER $

CREATE TRIGGER on_create_deterioration
BEFORE INSERT ON deteriorations
FOR EACH ROW
BEGIN
  CALL check_if_not_in_destructions(NEW.id);
END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       TRIGGER : on_create_involvement
# -----------------------------------------------------------------------------

DROP TRIGGER IF EXISTS on_create_involvement;

DELIMITER $

CREATE TRIGGER on_create_involvement
BEFORE INSERT ON vehicules_accidents
FOR EACH ROW
BEGIN
  DECLARE is_in_vehicules_sinistres BIT DEFAULT 0;
  SELECT IF(COUNT(*) > 0, 1, 0)
  INTO is_in_vehicules_sinistres
  FROM vehicules_sinistres
  WHERE vehicule_id = NEW.vehicule_id AND sinistre_id = NEW.accident_id;

  IF NOT is_in_vehicules_sinistres THEN
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'Cannot insert into `vehicules_accidents`, primary key not found in `vehicules_sinistres` !';
  END IF;
END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       TRIGGER : on_delete_registration
# -----------------------------------------------------------------------------

DROP TRIGGER IF EXISTS on_delete_registration;

DELIMITER $

CREATE TRIGGER on_delete_registration
AFTER DELETE ON immatriculations
FOR EACH ROW
BEGIN
  INSERT INTO h_immatriculations
  VALUES(OLD.id, OLD.code, OLD.assure_id, OLD.carte_grise, OLD.date_achat, OLD.type, OLD.maj, NULL);
END $

DELIMITER ;

# -----------------------------------------------------------------------------
#       TRIGGER : on_delete_contract
# -----------------------------------------------------------------------------

DROP TRIGGER IF EXISTS on_delete_contract;

DELIMITER $

CREATE TRIGGER on_delete_contract
AFTER DELETE ON contrats
FOR EACH ROW
BEGIN
  INSERT INTO h_contrats
  VALUES(OLD.id, OLD.immat_id, OLD.type_garantie_id, OLD.montant, OLD.date_souscription, OLD.etat, OLD.contrat, OLD.maj, NULL);
END $

DELIMITER ;