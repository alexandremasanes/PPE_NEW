<?php require_once path('header') ?>
<?php require_once path('navbar') ?>
<?php require_once path('banner') ?>
<div>
    <?php if(isset($sinisters)): ?>
        <table class="table-striped">
            <tr>
                <td class="col-md-3">ID</td>
                <td class="col-md-3">Type</td>
                <td class="col-md-3">Description</td>
                <td class="col-md-3">Date</td>
                <td class="col-md-3">Heure</td>
                <td class="col-md-3">Verifie</td>
            </tr>
            <?php foreach($sinisters as $sinister): ?>
                <tr>
                    <td class="col-md-3"><?=$sinister->id?></td>
                    <td class="col-md-3"><?=$sinister->type?></td>
                    <td class="col-md-3"><?=$sinister->description?></td>
                    <td class="col-md-3"><?=$sinister->date?></td>
                    <td class="col-md-3"><?=$sinister->time?></td>
                    <td class="col-md-3"><?=$sinister->checked?'oui':'non'?></td>
                </tr>
            <?php endforeach ?>
        </table>
    <?php endif ?>
</div>
<?php require_once path('footer') ?>