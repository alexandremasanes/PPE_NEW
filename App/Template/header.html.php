<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?=$head_title??''?> | Assurance Automobile Aixoise</title>
        <base href="<?=WEBROOT?>">
        <link href="<?=WEBROOT?>Public/StyleSheet/default.css" rel="stylesheet" type="text/css" media="all" />
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
        <?php if(isset($style_sheets)): ?>
            <?php foreach ($style_sheets as $style_sheet): ?>
                <link href="<?=WEBROOT.$style_sheet?>" rel="stylesheet" type="text/css" media="all" />
            <?php endforeach ?>
        <?php endif ?>
    </head>
    <body>
        <!--googleoff: index-->
        <noscript>
            <meta http-equiv="refresh" content="0;URL=<?=WEBROOT?>erreur/600/">
        </noscript>
        <!--googleon: index-->
