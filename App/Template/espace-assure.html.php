<?php require_once path('header') ?>
<?php require_once path('navbar') ?>
<?php require_once path('banner') ?>
    <div id="container">
        <div id="banner">
            <div class="image-border">
                <a href="#">
                    <img src="<?=WEBROOT?>Public/Images/cheap-car-insurance-quotes-4.jpg" width="870" height="253" alt=""
                         class="img-responsive"/>
                </a>
            </div>
        </div>
        <div class="enable-menu">
            <ul>
                <li><a href="<?=WEBROOT?>espace-assure/#customer_board">Board</a></li>
                <li><a href="<?=WEBROOT?>espace-assure/#contracts_list">Contrat</a></li>
                <li><a href="<?=WEBROOT?>espace-assure/#contrat_form">Contrat</a></li>
            </ul>
        </div>
        <div id="customer_board" class="panel panel-default col-md-8">
            <div class="panel-heading" style="background: #353535; color: #AFAFAF;">
            </div>
            <div  class="panel-body" style="height: 1000px;">
                <div class="post"><br/>
                    <div class="entry">
                        <?php if (isset($blocks)): ?>
                            <?php foreach ($blocks as $block): ?>
                                <div class="container">
                                    <?php require_once path($block); ?>
                                </div>
                            <?php endforeach ?>
                        <?php endif ?>
                        <?php if (isset($selection) && $selection->get(ETAT) == Contrat::VALIDE): ?>
                        <h2>
                            Sinistres:
                        </h2>
                        <div id="sinistres" style="width: 100% ; overflow: auto;">
                            <?php include 'JQtest.php'; ?>
                        </div>
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">
                            <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="contracts_list" class="panel panel-default col-md-4">
            <div class="panel-heading" style=" background: #353535; color: #AFAFAF;">
                <label class="title" style="width: 300px;">
                    Vos contrats
                </label>
            </div>
            <div class="panel-body list-group">
                <ul style="width: 200px; height: 200px; overflow: auto">
                    <?php if ($contracts): ?>
                        <?php foreach ($contracts as $key => $cont): ?>
                            <?php switch($key):
                                case $index??-1: ?>
                                    <li class="list-group-item active">
                                        <a href="<?=WEBROOT?>espace-assure/contrat/<?= $key + 1 ?>/"
                                           style="color :<?= $cont['couleur'] ?>;">
                                            <?= $cont['libelle'] ?>
                                        </a>
                                    </li>
                                    <?php break?>
                                <?php case 0: ?>
                                    <li class="first list-group-item">
                                        <a href="<?=WEBROOT?>espace-assure/contrat/<?= $key + 1 ?>/"
                                           style="color :<?= $cont['couleur'] ?>;">
                                            <?= $cont['libelle'] ?>
                                        </a>
                                    </li>
                                    <?php break?>
                                <?php default : ?>
                                <li class="list-group-item">
                                    <a href="<?= WEBROOT ?>espace-assure/contrat/<?= $key + 1 ?>/"
                                       style="color :<?= $cont['couleur'] ?>;">
                                        <?= $cont['libelle'] ?>
                                    </a>
                                </li>
                                <?php break?>
                            <?php endswitch ?>
                        <?php endforeach ?>
                    <?php endif ?>
                </ul>
            </div>
        </div>
        <div  id="contrat_form" class="panel panel-default col-md-4">
            <div class="panel-heading" style=" background: #353535; color: #AFAFAF;">
                <label class="title" style="width: 300px;">
                    Nouveau contrat
                </label>
            </div>
            <div class="panel-body">
                <form name="newcontrat" method="POST" enctype="multipart/form-data"
                      action="<?=WEBROOT?>nouveau-contrat/">
                    <div class="form-group">
                        <label>
                            Type de garantie :
                        </label>
                        <select class="form-control" id="insurances" size="1">
                            <option>
                        </select>
                        <div class="form-group">
                            <label>
                                Police annuelle de base :
                            </label>
                            <div class="input-group">
                                <input class="form-control" id="police" readonly>
                                <span class="input-group-addon">€</span>
                            </div>
                            <input id="code" name="code" style="display: none;" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            Marque du véhicule :
                        </label>
                        <input class="ui-widget form-control" id="make">
                        <input class="" id="make_id" value="" hidden>
                    </div>
                    <div class="form-group">
                        <label>
                            Modèle et année du véhicule :
                        </label>
                        <input class="ui-widget form-control" id="model_year">
                        <input id="model_id" name="model_id" value="" hidden><br/>
                    </div>
                    <div class="form-group">
                        <label>
                            Date d'achat :
                        </label><br/>
                        <input class="form-control" id="purchase_date" name="purchase_date" type="datetime">
                    </div>
                    <div class="form-group">
                        <label>
                            Numéro de série :
                        </label>
                        <input class="form-control" name="vin">
                    </div>
                    <div class="form-group">
                        <label>
                            Immatriculation du véhicule :
                        </label>
                        <input class="form-control" name="registration">
                    </div>

                    <div class="form-group">
                        <p>
                            <label>
                                Carte grise du véhicule :
                            </label>
                            <input name="card" type="file">
                        </p>
                    </div>
                    <input type="submit" value="Valider">
                </form>
            </div>
        </div>
    </div>
<?php require_once path('footer') ?>