<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand">AAA</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li <?=($_SERVER["REQUEST_URI"] == WEBROOT) ? 'class="active"':''?>><a href="<?=WEBROOT?>" accesskey="1" title=""><span>Accueil</span></a></li>
                <li <?=($_SERVER["REQUEST_URI"] == WEBROOT.'nos-services/')?'class="active"':''?>><a href="<?=WEBROOT?>nos-services/" accesskey="2" title=""><span>Services</span></a>
                <li <?=($_SERVER["REQUEST_URI"] == WEBROOT.'press.php')? 'class="active"':''?>><a href="#" accesskey="4" title=""><span>Press</span></a>
                <li <?=($_SERVER["REQUEST_URI"] == WEBROOT.'a-propos/')?'class="active"':''?>><a href="<?=WEBROOT?>a-propos/" accesskey="3" title=""><span>À Propos</span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if($link1): ?>
                    <li>
                        <a href='<?=$link1->url?>'>
                            <span>
                                <?=$link1->name?>
                            </span>
                        </a>
                    </li>
                <?php endif ?>
                <?php if($link2): ?>
                    <li role="presentation">
                        <a href='<?=$link2->url?>'>
                            <span>
                                <?=$link2->name?>
                            </span>
                        </a>
                    </li>
                <?php endif ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>