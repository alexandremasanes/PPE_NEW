<?php require_once path('header') ?>
<?php require_once path('navbar') ?>
<?php require_once path('banner') ?>
	<div id="container">
		<div id="banner">
			<div class="image-border">
				<a href="#">
					<img src="<?=WEBROOT?>Public/Images/provence_ban.jpg" class="img-responsive" alt="" />
				</a>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading" style = " background: #353535; color: #AFAFAF;">
				<h3 class="panel-title">
					Bienvenue
				</h3>
			</div>
			<div class="panel-body">
				<q>
					<i>
						Ce n'est pas parce qu'assurer son véhicule est obligatoire que ça ne peut être agréable.
					</i>
				</q>
				<br/>
				<br/>
				Assurance Automobile Aixoise vous propose de souscrire à des contrats en ligne.
				<br/>
				Nos offres sont détaillées dans la rubriques services.
				<br/>
				Bonne navigation.
			</div>
		</div>
	</div>
<?php require_once path('footer') ?>