<?php

$class_loader = function ($class_name) { 
    $name_space_class_name = explode('\\',$class_name);
    $name_space = reset($name_space_class_name);
    $class_name = end($name_space_class_name);
    if($class_name == 'TCPDF'){
        if (file_exists($path = __DIR__ . '/Library/TCPDF/' . $class_name . '.php'))
            require_once $path;
    }else if(in_array($class_name, ['Input', 'Session', 'Log', 'Mail', 'Query', 'Search',
                                    'ArraySet', 'Configuration', 'File', 'Singleton', 'Context',
                                    'ApiContext', 'AppContext', 'ContextImpl', 'RouteFactory',
                                    'Route', 'Form', 'FormField'])) {
        if (file_exists($path = __DIR__.'/Core/Misc/' . $class_name . '.php'))
            require_once $path;

    }else if(strpos($class_name, 'Exception') !== FALSE) {
        if (file_exists($path = __DIR__ . '/Core/Exception/' . $class_name . '.php'))
            require_once $path;

    }else if(strpos($class_name, 'Controller') !== FALSE || $class_name === 'Dispatcher') {
        if (file_exists($path = __DIR__ . '/Core/Controller/' . $class_name . '.php'))
            require_once $path;
    }else if(in_array($class_name, ['Response','View','Redirection','Download','ErrorView','JsonResponse'])) {
        if (file_exists($path = __DIR__ . '/Core/Response/' . $class_name . '.php'))
            require_once $path;
    }else if(strpos($class_name, 'DAO') !== FALSE) {
        if (file_exists($path = __DIR__ . '/Core/DAO/' . $class_name . '.php'))
            require_once $path;
    }else if(strpos($class_name, 'Service') !== FALSE) {
        if (file_exists($path = __DIR__ . '/Core/Service/' . $class_name . '.php'))
            require_once $path;
    }else if(strpos($class_name, 'Request') !== FALSE){
        if (file_exists($path = __DIR__ . '/Core/PostRequest/' . $class_name . '.php'))
            require_once $path;
    }else {
        //$dir = ($name_space == 'EntityDAO')? 'entityDAO' : 'entityORM';
        if (file_exists($path = __DIR__ . '/Core/Entity/' . $class_name . '.php'))
            require_once $path;
    }

};

spl_autoload_register($class_loader);
