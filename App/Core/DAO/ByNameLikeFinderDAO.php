<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 18/01/2017
 * Time: 11:15
 */
interface ByNameLikeFinderDAO{

    public function findByNameLike(string $name): ArraySet;
}