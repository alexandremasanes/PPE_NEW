<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 25/10/2016
 * Time: 15:11
 */
class ModelDAO extends DAO implements ByNameLikeFinderDAO{
    const
        SUBJECT_CLASS = Model::class;

    public function findByNameLike(string $name): ArraySet{
        $repository = self::$entity_manager->getRepository(self::SUBJECT_CLASS);
        $query_builder = self::$entity_manager->createQueryBuilder('m');
        $query = $query_builder->where('m.name LIKE \''.$name.'%\'')
                               ->getQuery();
        return new ArraySet(static::SUBJECT_CLASS, $query->getResult());
    }

    public function findByNameLikeAndMakeId(string $name, int $make_id): ArraySet{
        $repository = self::$entity_manager->getRepository(self::SUBJECT_CLASS);
        $query_builder = $repository->createQueryBuilder('mo');
        $query = $query_builder->select('mo')
                               ->leftJoin('mo.make', 'ma')
                               ->where('ma.id ='.$make_id)
                               ->andWhere('mo.name LIKE \''.$name.'%\'')
                               ->getQuery();
        return new ArraySet(static::SUBJECT_CLASS, $query->getResult());
    }
}