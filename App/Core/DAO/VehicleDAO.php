<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 19/06/2016
 * Time: 15:21
 */
class VehicleDAO extends DAO{
    const 
        SUBJECT_CLASS = Vehicle::class,
        TABLE = 'vehicules';
    
    protected function __construct(){
        parent::__construct();
    }
    
    public function create(Entity $object): bool{
        assert($object instanceof Vehicle);

        $query = self::$connection->query("SELECT count(*) FROM vehicule 
                                           WHERE immatriculation ='".$object->get(IMMATRICULATION)."'");
        if($query->fetch()['count(*)']!=0)
            return FALSE;
        $query = self::$connection->query("SELECT AUTO_INCREMENT as nextId FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".self::$db_name."' AND TABLE_NAME = 'vehicule'");
        $id = $query->fetch()['nextId'];
        $object->set(ID_VEHICULE,$id);
        $cg = explode('&ext=', $object->get(CARTE_GRISE));
        $temp = $cg[0]??"";
        $ext = $cg[1]??"";
        if (!rename($temp, DOCUMENT_ROOT.WEBROOT."Storage/CartesGrises/cg". $id . "." .$ext))
            return FALSE;
        $object->set(CARTE_GRISE,WEBROOT."Storage/CartesGrises/cg". $id);
        self::$connection->query('INSERT INTO vehicule(immatriculation,carte_grise,id_assure)
                                  VALUES("'.$object->get(IMMATRICULATION).'","'.$object->get(CARTE_GRISE).'",'.$object->getAssure()->get(ID_ASSURE).')');
        return true;

    }

    public function save(Entity $object): bool{
        assert($object instanceof Vehicle);
        assert($object->changedFields());

        $stm = "UPDATE vehicule SET ";
        foreach($object->changedFields() as $k => $v) {
            if (is_numeric($v))
                $stm .= "$k = $v,";
            else
                $stm .= "$k = '$v',";
            unset($object->changedFields()[$k]);
        }
        $stm[strlen($stm)-1] = "";
        self::$connection->prepare($stm)
                         ->execute();
        return TRUE;

    }

    public function remove(Entity $object): bool{
        // TODO: Implement delete() method.
    }
}