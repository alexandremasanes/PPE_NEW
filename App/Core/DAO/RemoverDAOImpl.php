<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 18/01/2017
 * Time: 11:43
 */
trait RemoverDAOImpl{

    public function remove(Entity... $entities): void{
        self::$entity_manager->remove($entities);
    }
}