<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 19/06/2016
 * Time: 13:49
 */

use \Doctrine\ORM\EntityManager;

abstract class DAO{
    const
        NO_AUTO_ID       = -1,
        INSUREE_DAO      = Insuree::SID,
        CONTRACT_DAO     = Contract::SID,
        VEHICLE_DAO      = Vehicle::SID,
        SINISTER_DAO     = Sinister::SID,
        REPARATION_DAO   = Reparation::SID,
        INSURANCE_DAO    = Insurance::SID,
        PERSON_DAO       = Person::SID,
        CUSTOMER_DAO     = Customer::SID,
        USER_ACCOUNT_DAO = UserAccount::SID,
        THIRD_PARTY_DAO  = ThirdParty::SID,
        ADMIN_DAO        = Admin::SID,
        EXPERT_DAO       = Expert::SID,
        DEALER_DAO       = Dealer::SID,
        MAKE_DAO         = Make::SID,
        MODEL_DAO        = Model::SID,
        DAMAGE_DAO       = Damage::SID,
        INDEMNITY_DAO    = Compensation::SID,
        ASSIGNMENT_DAO   = Assignment::SID,
        REFRESH_SCALE    = 'MINUTE',
        SUBJECT_CLASS    = NULL;
    
    protected static
        $instances       = NULL,
        $entity_manager  = NULL,
        $in_transaction  = FALSE;
    protected
        $table           = NULL;

    public static function get(int $DAO): DAO{
        assert(in_array($DAO,[self::INSUREE_DAO,
                              self::CONTRACT_DAO,
                              self::VEHICLE_DAO,
                              self::SINISTER_DAO,
                              self::REPARATION_DAO,
                              self::INSURANCE_DAO,
                              self::MODEL_DAO,
                              self::MAKE_DAO,
                              self::ADMIN_DAO,
                              self::CUSTOMER_DAO,
                              self::USER_ACCOUNT_DAO,
                              self::DEALER_DAO,
                              self::INDEMNITY_DAO,
                              self::PERSON_DAO,
                              self::THIRD_PARTY_DAO,
                              self::DAMAGE_DAO,
                              self::EXPERT_DAO,
                              self::ASSIGNMENT_DAO]),
               new AssertionError((__METHOD__.' : Invalid Argument !')));

        if(!self::$instances)
            self::$instances = new ArraySet(DAO::class);

        if(isset(self::$instances[$DAO]))
            return self::$instances[$DAO];

        switch($DAO){
            case self::INSUREE_DAO:
                self::$instances[$DAO] = new InsureeDAO;
                break;

            case self::CONTRACT_DAO:
                self::$instances[$DAO] = new ContractDAO;
                break;

            case self::VEHICLE_DAO:
                self::$instances[$DAO] = new VehicleDAO;
                break;

            case self::SINISTER_DAO:
                self::$instances[$DAO] = new SinisterDAO;
                break;

            case self::REPARATION_DAO:
                self::$instances[$DAO] = new ReparationDAO;
                break;

            case self::INSURANCE_DAO:
                self::$instances[$DAO] = new InsuranceDAO;
                break;

            case self::USER_ACCOUNT_DAO:
                self::$instances[$DAO] = new UserAccountDAO;
                break;

            case self::ASSIGNMENT_DAO:
                self::$instances[$DAO] = new AssignmentDAO;
                break;

            case self::CUSTOMER_DAO:
                self::$instances[$DAO] = new CustomerDAO;
                break;

            case self::PERSON_DAO:
                self::$instances[$DAO] = new PersonDAO;
                break;

            case self::THIRD_PARTY_DAO:
                self::$instances[$DAO] = new ThirdPartyDAO;
                break;

            case self::ADMIN_DAO:
                self::$instances[$DAO] = new AdminDAO;
                break;

            case self::MAKE_DAO:
                self::$instances[$DAO] = new MakeDAO;
                break;

            case self::MODEL_DAO:
                self::$instances[$DAO] = new ModelDAO;
                break;
        }
        return self::$instances[$DAO];
    }

    protected function __construct(){}

    protected function getTable(): string{
        return self::$entity_manager->getClassMetadata(static::SUBJECT_CLASS)->getTableName();
    }

    public static function saveFile(string $old_path, string $ext): string{
        $old_name = basename($old_path);
        $set      = array_merge(range('0','9'), range('A','Z'), range('a','z'));
        $len      = strlen($old_name);
        do{
            $set      = array_rand($set,$len);
            $new_name = implode($set);
            $new_path = STORAGE_ROOT.$new_name.'.'.$ext;
        }while(file_exists($new_path));

        return (rename($old_path,$new_path))?$new_path:'';
    }

    public function findOneBy(array $criterias): Entity{
        $repository = self::$entity_manager->getRepository(static::SUBJECT_CLASS);
        return $repository->findOneBy($criterias);
    }

    public function findBy(array $criterias, int $limit = 0): ArraySet{

        $repository = self::$entity_manager->getRepository(static::SUBJECT_CLASS);
        if($limit)
            $results =  $repository->findBy($criterias, $limit);
        else
            $results = $repository->findBy($criterias);
        return new ArraySet(static::SUBJECT_CLASS, $results);
    }

    public function findAll(): ArraySet{
        $repository = self::$entity_manager->getRepository(static::SUBJECT_CLASS);
        return new ArraySet(static::SUBJECT_CLASS, $repository->findAll());
    }
    
    public function __clone(){
        throw new RuntimeException(static::class.' is not clonable !');
    }
    
    public static function init(EntityManager $entity_manager): void{
       self::$entity_manager = $entity_manager;
    }
}