<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 18/01/2017
 * Time: 11:07
 */
interface RemoverDAO{

    public function remove(Entity... $entities): void;
}