<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 22/10/2016
 * Time: 13:56
 */
final class AdminDAO extends DAO{
    const
        SUBJECT_CLASS=Admin::class;
    
    protected static
        $fields=[];

    public function find(int $limit = 0, Search ...$searchs): ArraySet{
        $objects = new ArraySet(self::SUBJECT_CLASS);
        $person_fields=PersonDAO::getFields();
        $admin_fields=self::getFields();
        $pk=self::getPrimaryKeys();
        $pf=key($pk);
        $pk=current($pk)['field'];
        $fields=array_merge($person_fields,$admin_fields);
        $selected_fields=self::extractColumnsNames($fields);

        $query= new Query;
        call_user_func_array([$query,'select'],$selected_fields);
        $query->from(self::TABLE)
              ->leftJoin(PersonDAO::TABLE,$pk,'=',$pk);
        call_user_func_array([$query,'where'],$searchs);
        $query->limit($limit);

        $query = self::$connection->query($query);
        if(!$query->rowCount())
            return $objects;

        while($results = $query->fetch(PDO::FETCH_ASSOC)) {
            $object = new Admin;
            foreach($results as $key=>$result){
                if(!$result)
                    continue;
                if(strpos($key,'.'))
                    $key=explode(',',$key)[1];
                $prop = self::getPropertyName($key);
                $object->$prop = $result;
            }
            $objects[]=$object;
        }
        return $objects;
    }
    
    public function save(Entity $object): bool{
        // TODO: Implement save() method.
    }

    public function remove(Entity $object): bool{
        // TODO: Implement remove() method.
    }
}