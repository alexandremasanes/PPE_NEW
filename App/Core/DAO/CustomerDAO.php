<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 17/10/2016
 * Time: 19:24
 */

class CustomerDAO extends PersonDAO implements RecorderDAO, RemoverDAO, HydratorDAO{
    use RecorderDAOImpl, RemoverDAOImpl;
    const
        SUBJECT_CLASS = Customer::class;

    public function hydrate(Hydratable & $hydratable): void{
        $repository = self::$entity_manager->getRepository(self::SUBJECT_CLASS);
        $query_builder = $repository->createQueryBuilder('c');
        $query_builder->select('c')
                      ->where('c.id = '.$hydratable->getId())
                      ->setMaxResults(1);
        $result = $query_builder->getQuery()
                                ->getResult();// die(print_r($query_builder->getQuery()));
        if(!$result)
            $hydratable = NULL;
        else{
            $hydratable->hydrate($result[0]);
            $hydratable = self::$entity_manager->merge($hydratable); //die('kek'.\Doctrine\Common\Util\Debug::dump($hydratable).'lol');
        }

    }
}