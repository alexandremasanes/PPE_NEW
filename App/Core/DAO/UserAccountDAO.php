<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 17/10/2016
 * Time: 19:33
 */
final class UserAccountDAO extends DAO implements RecorderDAO, RemoverDAO{
    use RecorderDAOImpl, RemoverDAOImpl;
    const
        SUBJECT_CLASS = UserAccount::class;
    
    public function findByEmailAndPwd(string $email, string $pwd) : ? UserAccount {
        $hash = sha1($email.UserAccount::SALT.$pwd);
        $user_account = self::$entity_manager->getRepository(UserAccount::class)
                                             ->findOneBy(['email'=>$email,'hash' =>$hash]);
        return $user_account;
    }

    public function isEmailUnique(string $email): bool{
        $search = (new Search)->add('email', $email);
        $query = (new Query)->select(1)
                            ->from($this->getTable())
                            ->where($search)
                            ->limit(1);

        return (self::$entity_manager->createNativeQuery($query, new \Doctrine\ORM\Query\ResultSetMapping)->getArrayResult())
            ?FALSE
            :TRUE;
    }

    public function doIdAndTokenMatch(int $id, string $token): bool{
        $search = (new Search)->add('id', $id)
                              ->add('token', $token);

        $query = (new Query)->select(1)
                            ->from($this->getTable())
                            ->where($search)
                            ->limit(1);

        return (self::$entity_manager->createNativeQuery($query, new \Doctrine\ORM\Query\ResultSetMapping)->getArrayResult())
            ?FALSE
            :TRUE;
    }
}