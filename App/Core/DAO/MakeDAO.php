<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 24/10/2016
 * Time: 17:13
 */
class MakeDAO extends DAO implements ByNameLikeFinderDAO {
    const
        SUBJECT_CLASS = Make::class;


    public function findByNameLike(string $name): ArraySet{
        $query = self::$entity_manager->getRepository(Make::class)
                                      ->createQueryBuilder('m')
                                      ->where('m.name LIKE \''.$name.'%\'')
                                      ->getQuery();

        return new ArraySet(Make::class, $query->getResult());
    }
}