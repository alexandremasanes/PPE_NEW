<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 18/01/2017
 * Time: 11:30
 */
interface HydratorDAO{

    public function hydrate(Hydratable & $hydratable): void;
}