<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 05/01/2017
 * Time: 16:30
 */
interface User{

    public function flushNotifs(): array;

    public function addNotif(string $notif): void;
}