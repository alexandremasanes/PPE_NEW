<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 04/01/2017
 * Time: 18:24
 */
trait RegisteredUserImpl{
    use UserImpl;

    public function getUserAccount(): ? UserAccount{
        return $this->user_account;
    }

    public function setUserAccount(? UserAccount $user_account): void{
        $user_account->setUser($this);
        $this->user_account = $user_account;
    }

    public function __sleep(): array{
        $properties = [];
        foreach(get_object_vars($this) as $property => $value)
            if(!is_object($value))
                $properties[] = $property;
        return $properties;
    }
}