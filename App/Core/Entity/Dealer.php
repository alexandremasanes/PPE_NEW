<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 16/10/2016
 * Time: 19:26
 */

/**
 * @Entity
 * @Table(name="garagistes")
 */
class Dealer extends Entity{
    const
        SID = 0xB96F;

    protected
        /**
         * @OneToMany(targetEntity="Deterioration", mappedBy="dealer", fetch="LAZY", cascade={"all"})
         */
        $deteriorations = NULL,
        /**
         * @Id @Column(type="integer") @GeneratedValue
         */
        $id = 0,
        /**
         * @Column(name="nom")
         */
        $name = '',
        /**
         * @Column(name="agree", type="boolean")
         */
        $is_certified = FALSE;

    public function __construct(){
        parent::__construct();
        $this->deteriorations = new ArraySet(Deterioration::class);
    }
    
    public function getDamages(): ArraySet{
        return self::toArraySet($this->deteriorations);
    }

    public function addDeterioration(Deterioration $deterioration): bool{
        return $this->deteriorations->add($deterioration);
    }

    public function removeDeterioration(Deterioration $deterioration): bool{
        return $this->deteriorations->removeElement($deterioration);
    }
    
    public function getId(): int{
        return $this->id;
    }
    
    public function setName(string $name): void{
        $this->name = $name;
    }
    
    public function getName(): string{
        return $this->name;
    }
}