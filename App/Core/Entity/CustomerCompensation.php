<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 03/02/2017
 * Time: 18:40
 */

/**
 * @Entity
 * @Table(name="indemnisations_clientes")
 */
class CustomerCompensation extends Compensation {
    protected
        /**
         * @ManyToOne(targetEntity="Customer",
         *            inversedBy="compensations",
         *            fetch="LAZY")
         * @JoinColumn(name="client_id", referencedColumnName="id")
         */
        $compensable;

    public function __construct(Customer $customer){
        $this->compensable = $customer;
    }

    public function getCustomer(): Customer{
        return $this->compensable;
    }
}