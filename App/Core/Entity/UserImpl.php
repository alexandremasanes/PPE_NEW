<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 16/10/2016
 * Time: 22:01
 */

trait UserImpl{
    protected
        $notifs = [];
    
    public function flushNotifs() : array{
        $notifs = $this->notifs;
        $this->notifs = [];
        return $notifs;
    }

    public function addNotif(string $notif): void{
        if(!in_array($notif, $this->notifs))
            $this->notifs[] = $notif;
    }
}