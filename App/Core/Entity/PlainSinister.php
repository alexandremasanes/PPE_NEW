<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 26/11/2016
 * Time: 17:31
 */

/**
 * @Entity
 * @Table(name="sinistres_sans_tiers")
 */

class PlainSinister extends Sinister{
    const
        SID = 0x718E;
    protected
        /**
         * @ManyToOne(targetEntity="PlainSinisterType", inversedBy="plain_sinisters", cascade={"all"})
         * @JoinColumn(name="type_id", referencedColumnName="id", nullable=false)
         */
        $type;
    
    public function __construct(PlainSinisterType $type, Vehicle $vehicle, ? Expert $expert = NULL){
        parent::__construct($vehicle, $expert);
        $this->type = $type;
    }

    public function getType(): PlainSinisterType{
        return $this->type;
    }

}