<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 15/01/2017
 * Time: 14:15
 */
interface Hydratable{

    public function hydrate(Hydratable $data): void;
}