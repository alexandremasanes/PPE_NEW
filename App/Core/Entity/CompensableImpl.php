<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 03/02/2017
 * Time: 15:45
 */
trait CompensableImpl{

    public function addCompensation(Compensation $indemnity): bool{
        return $this->compensations->add($indemnity);
    }

    public function removeCompensation(Compensation $indemnity): bool{
        return $this->compensations->removeElement($indemnity);
    }

    public function getCompensations(): ArraySet{
        return static::toArraySet($this->compensations);
    }
}