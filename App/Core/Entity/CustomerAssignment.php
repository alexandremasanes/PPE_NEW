<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 24/01/2017
 * Time: 11:10
 */

/**
 * @Entity
 */
class CustomerAssignment extends Assignment {
    protected
        /**
         * @ManyToOne(targetEntity="Customer", inversedBy="assignments", fetch="EAGER")
         * @JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
         */
        $customer = NULL;

        public function __construct(Customer $customer, Admin $admin, Status $status){
            parent::__construct($admin, $status);
            $this->customer = $customer;
        }
}