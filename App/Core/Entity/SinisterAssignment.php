<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 24/01/2017
 * Time: 11:13
 */

/**
 * @Entity
 */
class SinisterAssignment extends Assignment{
    protected
        /**
         * @ManyToOne(targetEntity="Sinister", inversedBy="assignments", fetch="EAGER")
         * @JoinColumn(name="sinister_id", referencedColumnName="id", nullable=false)
         */
        $sinister = NULL;

        public function __construct(Sinister $sinister, Admin $admin, Status $status){
            parent::__construct($admin, $status);
            $this->sinister = $sinister;
        }
}