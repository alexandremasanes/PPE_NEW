<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 16/10/2016
 * Time: 16:47
 */

/**
 * @Entity
 * @Table(name="personnes")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="type", type="integer")
 * @DiscriminatorMap({
 *     1="Admin",
 *     2="Expert",
 *     3="Customer",
 *     4="ThirdParty"
 * })
 */

abstract class Person extends Entity{
    const
        SID = 0xEAAB;
    protected
        /**
         * @Id @Column(type="integer") @GeneratedValue
         */
        $id,
        /**
         * @Column(name="prenom")
         */
        $first_name,
        /**
         * @Column(name="nom")
         */
        $last_name,
        /**
         * @Column(name="adresse")
         */
        $address,
        /**
         * @Column(name="ville")
         */
        $city,
        /**
         * @Column(name="code_postal", type="integer")
         */
        $zip_code,
        /**
         * @Column(name="tel")
         */
        $tel_number,
        /**
         * @OneToOne(targetEntity="UserAccount", mappedBy="user", fetch="EAGER", cascade={"all"})
         */
        $user_account;

    public function getId(): int{
        return $this->id ?? 0;
    }

    public function setFirstName(string $first_name): void{
        $this->first_name = $first_name;
    }

    public function getFirstName(): string{
        return $this->first_name ?? '';
    }

    public function setLastName(string $last_name): void{
        $this->last_name = $last_name;
    }

    public function getLastName(): string{
        return $this->last_name ?? '';
    }

    public function setAddress(string $address): void{
        $this->address = $address;
    }

    public function getAddress(): string{
        return $this->address ?? '';
    }

    public function setCity(string $city): void{
        $this->city = $city;
    }

    public function getCity(): string{
        return $this->city ?? 0;
    }

    public function setZipCode(int $zip_code): void{
        $this->zip_code = $zip_code;
    }

    public function getZipCode(): int{
        return $this->zip_code ?? 0;
    }

    public function setTelNumber(string $tel_number): void{
        $this->tel_number = $tel_number;
    }

    public function getTelNumber(): string{
        return $this->tel_number ?? '';
    }
}