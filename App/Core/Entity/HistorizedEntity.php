<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 03/02/2017
 * Time: 10:14
 */

/**
 * @MappedSuperclass
 */
class HistorizedEntity extends Entity {

    protected
        /**
         * @Id @Column(name="date_h", type="datetime")
         */
        $created_at = NULL;

    public function __clone(){
        throw new Exception;
    }

    public function createdAt(): DateTime {
        return $this->created_at;
    }

    //NOT INSTANTIABLE !
    private function __construct(){}
}