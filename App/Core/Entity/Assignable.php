<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 24/01/2017
 * Time: 11:42
 */
interface Assignable{

    public function getAssignments(): ArraySet;

    public function addAssignment(Assignment $assignment): bool;

    public function removeAssignment(Assignment $assignment): bool;

    public function getLastAssignment(): Assignment;
}