<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 16/10/2016
 * Time: 17:22
 */

/**
 * @Entity
 * @Table(name="experts")
 */
class Expert extends Person{
    use RegisteredUserImpl;
    const
        SID = 0xA87;
    protected
        /**
         * @Column(name="rang", type="integer")
         */
        $rank = 0,
        /**
         * @OneToMany(targetEntity="Sinister", mappedBy="expert", fetch="LAZY")
         */
        $sinisters = NULL;


    public function __construct(){
        $this->sinisters = new ArraySet(Sinister::class);
    }

    public function setRank(int $rank): void{
        $this->rank = $rank;
    }
    
    public function getRank(): int{
        return $this->rank;
    }

    public function getSinisters(): ArraySet{
        return self::toArraySet($this->sinisters);
    }

    public function addSinister(Sinister $sinister): bool{
        return $this->sinisters->add($sinister);
    }

    public function removeSinister(Sinister $sinister): bool{
        $this->sinisters->removeElement($sinister);
    }
}