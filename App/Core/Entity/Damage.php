<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 16/10/2016
 * Time: 18:26
 */

/**
 * @Entity
 * @Table(name="dommages")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="type", type="integer")
 * @DiscriminatorMap({
 *     0="Deterioration",
 *     1="Destruction"
 * })
 */
abstract class Damage extends Entity{
    const
        SID         = 0x3F81,
        SPOILAGE    = 1,
        DESTRUCTION = 2;

    protected
        /**
         * @Id
         * @OneToOne(targetEntity="Sinister", inversedBy="damage", fetch="EAGER")
         * @JoinColumn(name="sinistre_id", referencedColumnName="id")
         */
        $sinister = NULL,
        $description,
        $amount;

    public function __construct(Sinister $sinister){
        $this->sinister = $sinister;
        $sinister->setDamage($this);
    }

    public function getSinister(): Sinister{
        return $this->sinister;
    }
    
    public function setDescription(string $description): void{
        $this->description = $description;
    }

    public function getDescription(): string{
        return $this->description;
    }

    public function setAmount(int $amount): void{
        $this->amount = $amount;
    }

    public function getAmount(): int{
        return $this->amount;
    }
}