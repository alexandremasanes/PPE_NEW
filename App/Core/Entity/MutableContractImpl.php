<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 03/02/2017
 * Time: 12:53
 */
trait MutableContractImpl{
    use ReadOnlyContractImpl;

    public function setAmount(float $amount){
        $this->amount = $amount;
    }

    public function setSubsDate(DateTime $subs_date): void{
        $this->subs_date = $subs_date;
    }

    public function setStatus(int $status): void{
        $this->status = $status;
    }

    public function setContract(string $contract): void{
        $this->contract = $contract;
    }
}