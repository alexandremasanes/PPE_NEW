<?php

interface Connectable{
    public function login(string $email, string $mdp);
}