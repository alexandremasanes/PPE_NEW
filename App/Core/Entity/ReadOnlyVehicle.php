<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 07/02/2017
 * Time: 14:18
 */
interface ReadOnlyVehicle {

    public function getModel(): Model;

    public function getSinisters(): ArraySet;

    public function getId(): int;

    public function getVin(): string;

    public function getInsuree(): ? Insuree;

    public function getContract(): ? Contract;

    public function getRegistrationNumber(): string;

    public function getRegistrationCertificate(): string;

    public function getValue(): float;
}