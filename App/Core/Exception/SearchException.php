<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 27/10/2016
 * Time: 15:22
 */
class SearchException extends NotFoundException{
    private
        $suspected_search;
    public function __construct($message, string $search){
        parent::__construct($message);
        $this->search=$search;
    }

    public function getSearch(): string{
        return $this->search;
    }
}