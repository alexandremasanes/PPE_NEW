<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 06/01/2017
 * Time: 18:23
 */
abstract class Service{
    protected 
        $context = NULL;
    
    public function __construct(Context $context){
        DAO::init($context->getConfiguration()->getEntityManager());
    }
}