<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 15/01/2017
 * Time: 15:07
 */
class AppService extends Service {
    private
        $customer_dao = NULL;

    public function __construct(AppContext $context){
        parent::__construct($context);
        $this->customer_dao = DAO::get(DAO::CUSTOMER_DAO);
    }

    public function initUser(Session $session): void{
        if(!$user = $session->get('user'))
            $session->set('user', new Guest);

        else if(!$user instanceof Guest) {
            $this->customer_dao->hydrate($user);
            if(!$user)
                $session->set('user', new Guest);
        }
    }
}