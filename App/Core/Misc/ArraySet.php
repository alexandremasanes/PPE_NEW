<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 18/11/2016
 * Time: 13:27
 */
use \Doctrine\Common\Collections\ArrayCollection;
class ArraySet extends ArrayCollection implements JsonSerializable {
    const
        KEY_TYPE = 'int';
    private
        $class;

    public function __construct(string $class, array $elements = []){
        assert(class_exists($class)||interface_exists($class)||trait_exists($class), new AssertionError);
        $this->class = $class;
        foreach($elements as $element)
            $this->assertClassValue($element);
        parent::__construct($elements);
    }

    public function offsetGet($offset){
        $this->assertOffsetType($offset);
        $this->checkIfOffsetExists($offset);
        return parent::offsetGet($offset);
    }

    public function offsetSet($offset, $value){
        $this->assertOffsetType($offset);
        $this->assertClassValue($value);
        if($this->contains($value))
            return;
        parent::offsetSet($offset,$value);
    }
    
    public function indexOf($value): int{
        foreach($this as $key => $data)
            if($data == $value)
                return $key;
        return -1;
    }

    public function contains($value): bool{
        foreach($this as $data)
            if($data == $value)
                return TRUE;
        return FALSE;
    }

    public function __toString(): string{
        $str = '[';
        if(!method_exists($this->class,'__toString'))
            foreach($this as $element)
                $str .= $this->class.'#'.spl_object_hash($element).';';
        else
            foreach($this as $element)
                $str.=($element??'NULL').';';

        $str[strlen($str)-1]=']';
        return $str;
    }

    public function jsonSerialize(): array{
        return $this->toArray();
    }

    private function assertOffsetType($offset){
        if($offset === NULL)
            return;
        assert(is_int($offset), new InvalidArgumentException('Offset type must '.self::KEY_TYPE.
                                                            ', '.gettype($offset).' here.'));
    }

    private function assertClassValue($value){
        assert($value instanceof $this->class, new InvalidArgumentException('Value must be an instance of '.$this->class));
    }

    private function checkIfOffsetExists(int $offset){
        if(!$this->offsetExists($offset))
            throw new OutOfBoundsException('Offset '.$offset.' doesn\'t exist.');
    }
}