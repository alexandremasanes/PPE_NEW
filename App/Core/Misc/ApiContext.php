<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 06/01/2017
 * Time: 09:11
 */

interface ApiContext extends Context{

    public function getConfiguration(): Configuration;
}