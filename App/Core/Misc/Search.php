<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 21/10/2016
 * Time: 14:57
 */
final class Search{
    private
        $criterias=[];
    
    public function add(string $field, $values): Search{
        assert(!is_object($values)&&!isset($this->criterias[$field]));
        if(strpbrk($values,'=*;()&#+?\\\'"`')!==FALSE)
            throw new SearchException('Suspected SQL injection !');
        $this->criterias[$field]=$values;
        return $this;
    }

    public function set(string $field, $values): Search{
        assert(isset($this->criterias[$field]));
        $this->criterias[$field]=$values;
        return $this;
    }

    public function rename(string $field, string $new_name): Search{
        $values = $this->criterias[$field];
        unset($this->criterias[$field]);
        $this->criterias[$new_name]=$values;
        return $this;
    }
    
    public function get(string $criteria): ? mixed{
        return $this->criterias[$criteria] ?? NULL;
    }

    public function getAll(): array{
        return $this->criterias;
    }
    
    public function __toString(): string{
        $str_arr=[];
        foreach($this->criterias as $criteria=>$values){
            $str=$criteria.' ';

            if(!is_array($values)) {
                $op='=';
                if(is_string($values)) {
                    $values = '"' . $values . '"';
                    $op='LIKE';
                }
                $str .= $op . $values;

            }else {
                foreach($values as &$value)
                    $value = ((!is_string($value))?$value:'"'.$value.'"');
                $str .= 'IN(' . implode(',', $values) . ')';
            }
                
             $str_arr[]=$str;   
        }
        return implode(' AND ',$str_arr);
    }
}