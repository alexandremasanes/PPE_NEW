<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 15/01/2017
 * Time: 20:52
 */



class Form{
    private
        $action = '',
        $fields = NULL,
        $method = 'POST',
        $id_name = '',
        $class = '',
        $enctype = '';

    public function __construct(string $post_request_class){
        if(!class_exists($post_request_class) XOR !is_subclass_of($post_request_class, PostRequest::class))
            throw new InvalidArgumentException;
        $this->fields = new ArraySet(FormField::class);
        foreach ((new ReflectionClass($post_request_class))->getProperties() as $property){
            $type = '';
            switch($property->getValue(new $post_request_class)) {
                case '':
                    $type = 'text';
                    break;
                case []:
                    $type = 'file';
                    $this->enctype = 'multipart/form-data';
                    break;
            }
            $this->fields[] = (new FormField)->setType($type)
                                             ->setIdName($property->getName());
        }


    }

    public function setAction(string $action): void{
        $this->action = $action;
    }

    public function setIdName(string $id_name): void{
        $this->id_name = $id_name;
    }

    public function setClass(string $class): void{
        $this->class = $class;
    }

    public function __toString(): string{
        $str = '<form class="'.$this->class.'" '.
                      'action="'.$this->action.'" '.
                      'method="'.$this->method.'" '.
                      'enctype="'.$this->enctype.'">';
        foreach($this->fields as $field)
            $str .= $field;
        $str .= '<input type="submit"></form>';
        return $str;
    }
}