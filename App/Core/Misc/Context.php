<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 17/01/2017
 * Time: 19:49
 */

interface Context{

    public function close(): void;
}