<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 13/01/2017
 * Time: 15:38
 */


final class Route{
    const
        GET    = 'GET',
        POST   = 'POST',
        PUT    = 'PUT',
        DELETE = 'DELETE',
        HEAD   = 'HEAD';
    private
        $pattern      = '',
        $methods      = [],
        $post_request = '',
        $controller   = '',
        $action       = '';

    public function match(string $url): ? array{
        if($url === $this->pattern)
            return [];
        else if (($url !== $this->pattern) && !$this->pattern)
            return NULL;
        $pattern = str_replace('-','\-',$this->pattern);
        if(!preg_match('#^'.$pattern.'$#', $url, $matches, PREG_OFFSET_CAPTURE))
            return NULL;
        unset($matches[0]);
        $params = [];
        foreach($matches as $match)
            $params[] = trim($match[0],'/');
        if(!$params)
            return NULL;
        return $params;
    }

    public function setPattern(string $pattern): void{
        $this->ensurePropIsNotSet('pattern');
        $this->pattern = $pattern;
    }

    public function getPattern(): string{
        return $this->pattern;
    }

    public function setMethods(array $methods): void{
        $this->ensurePropIsNotSet('methods');
        $this->methods = $methods;
    }

    public function getMethods(): array{
        return $this->methods;
    }

    public function setPostRequest(string $post_request): void{
        $this->ensurePropIsNotSet('post_request');
        $this->post_request = $post_request;
    }

    public function getPostRequest(): string{
        return $this->post_request;
    }

    public function setController(string $controller): void{
        $this->ensurePropIsNotSet('controller');
        $this->controller = $controller;
    }

    public function getController(): string{
        return $this->controller;
    }

    public function setAction(string $action): void{
        $this->ensurePropIsNotSet('action');
        $this->action = $action;
    }

    public function getAction(): string{
        return $this->action;
    }

    private function ensurePropIsNotSet(string $prop): void{
        if($this->$prop)
            throw new Exception;
    }
}