<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 17/01/2017
 * Time: 19:50
 */


interface AppContext extends ApiContext {

    public function getSession(): Session;
}