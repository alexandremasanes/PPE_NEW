<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 15/01/2017
 * Time: 18:10
 */

/**
 * @PostRequest
 */
class ContractUploadingRequest extends PostRequest{
    public
        /**
         * @Field(name="contract", type="text")
         */
        $tmp_file = [];
}