<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 15/01/2017
 * Time: 18:04
 */

/**
 * @PostRequest
 */
class LoggingInRequest extends PostRequest {
    public
        /**
         * @Field(type="email")
         */
        $email = '',
        /**
         * @Field(name="pwd", type="password")
         */
        $password = '';
}