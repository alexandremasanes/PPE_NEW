<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 15/01/2017
 * Time: 18:00
 */

abstract class PostRequest{
    private static
        $metadata = [];

    private function __construct(){}

    public static function getMetadata(): array{
        return self::$metadata;
    }

    public static function isEmpty(PostRequest $request): bool{
        foreach($request as $property => $value)
            if((!is_array($value) && !strlen(trim($value))) XOR !$value)
                continue;
            else
                return FALSE;
        return TRUE;
    }

    public static function map(string $request_class, Input $input): ? PostRequest{
        if(!is_subclass_of($request_class, self::class))
            return NULL;

        $request = new $request_class;
        self::parseFieldsAnnotations($request_class);
        foreach (self::$metadata[$request_class] as $item){
            $property_name = $item->property_name;
            $value = $input->get($item->field_name);
            if($value && $item->field_type == 'datetime')
                $value = new DateTime($value);
            $request->$property_name = $value;
        }
        return $request;
    }

    private static function parseFieldsAnnotations(string $request_class): void{
        if(apcu_exists($key = 'request.cache')){
            self::$metadata = apcu_fetch($key);
            if(isset(self::$metadata[$request_class]))
                return;
        }
        $reflect = new ReflectionClass($request_class);
        $fields  = [];
        foreach($reflect->getProperties() as $prop){
            $doc_comment = $prop->getDocComment();
            preg_match("/\@(\w+)\(?/", $doc_comment, $match_name);
            $annotation_name = $match_name[1];
            if($annotation_name != 'Field')
                continue;

            $fields[] = $metadata = new stdClass;
            $metadata->property_name = $prop->getName();
            $match_values = NULL;
            preg_match("/\(([\s\S]+)\)/", $doc_comment, $match_values);
            if(!$match_values){
                $metadata->field_name = $metadata->property_name;
                $metadata->field_type = 'text';

            }else{
                $params = [];
                $values = explode(',', $match_values[1]);
                foreach ($values as $value) {
                    $exploded = explode('=', $value);
                    $params[trim($exploded[0])] = trim($exploded[1], '"');
                }
                $metadata->field_name = $params['name'] ?? $prop->getName();
                $metadata->field_type = $params['type'] ?? 'text';
            }
        }
        self::$metadata[$request_class] = $fields;
        apcu_add('request.cache', self::$metadata);
    }
}