<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 03/07/2016
 * Time: 19:33
 */
final class Dispatcher{
    private static
        $context = NULL,
        $route = NULL,
        $parameters = NULL;
    
    private function __construct(){}

    public static function dispatch(string $url): void{
        $url = str_replace('main.php', '', $url);
        $configuration = Configuration::getInstance();
        RouteFactory::init($configuration->getCache());
        $collector = RouteFactory::getInstance();
        foreach ($collector->getRoutes() as $route)
            if((self::$parameters = $route->match($url)) !== NULL){
                self::$route = $route;
                self::$context = is_subclass_of($route->getController(), AppController::class)
                                 ? self::makeAppContext($configuration) 
                                 : self::makeApiContext($configuration);

                if(($methods = $route->getMethods()) && !in_array(REQUEST_METHOD, $methods))
                    throw new MethodNotAllowedException(
                        $route->getController(),
                        $route->getAction(),
                        $methods,
                        REQUEST_METHOD
                    );
                if(REQUEST_METHOD == Route::POST) {
                    if(!$post_request = PostRequest::map($route->getPostRequest(), Input::getInstance()))
                        throw new BadRequestException(
                            $route->getController(),
                            $route->getAction(),
                            $route->getPostRequest()
                        );
                    self::$parameters = array_merge([$post_request], self::$parameters);
                }
                return;
            }

        throw new RouteNotFoundException($url);
    }

    public static function deliver(): Response{
        $response = NULL;
        $controller = self::$route->getController();
        $controller = new $controller;
        $action = self::$route->getAction();
        $params = self::$parameters;

        if($controller instanceof ContextUsingController)
            $controller->use(self::$context);

        if($controller instanceof BeforeFilteringController
        && $response = $controller->beforeFilter())
            return $response;

        if($params)
                $response = (new ReflectionMethod($controller, $action))->invokeArgs($controller, $params);
        else
            $response = $controller->$action();

        return $response;
    }
    
    public static function close(): void{
        self::$context->close();
    }

    private static function makeAppContext(Configuration $configuration): ApiContext{
        return new class(
            $configuration,
            Session::getInstance()
        ) implements AppContext{
            private
                $session = NULL,
                $configuration = NULL;

            public function __construct(Configuration $configuration, Session $session){
                $this->configuration = $configuration;
                $this->session = $session;
            }

            public function close(): void{
                unset($this->session, $this->configuration);
            }

            public function getConfiguration(): Configuration{
                return $this->configuration;
            }

            public function getSession(): Session{
                return $this->session;
            }
        };
    }

    private static function makeApiContext(Configuration $configuration): ApiContext{
        return new class(
            $configuration
        ) implements ApiContext {
            private
                $configuration = NULL;

            public function __construct(Configuration $configuration){
                $this->configuration = $configuration;
            }

            public function close(): void{
                unset($this->configuration);
            }

            public function getConfiguration(): Configuration{
                return $this->configuration;
            }
        };
    }
}