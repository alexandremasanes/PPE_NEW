<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 10/07/2016
 * Time: 20:32
 */

/**
 * @Controller
 */
final class ErrorController extends Controller{

    /**
     * @Action
     * @Route(pattern="(\d+)/", methods={"GET"})
     */
    public function showError(int $code = Response::NOT_FOUND): ErrorView{
        return new ErrorView($code);
    }
}