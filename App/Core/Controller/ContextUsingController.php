<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 06/01/2017
 * Time: 09:19
 */

interface ContextUsingController{
	
    public function use(Context $context): void;
}