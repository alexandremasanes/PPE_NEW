<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 04/07/2016
 * Time: 20:04
 */

/**
 * @Controller
 */
final class ServicesController extends AppController{
    /**
     * @Action
     * @Route(pattern="nos-services/", methods={"GET"})
     */
    public function showIndex(): View{
        $title = 'Nos services';
        $view = $this->makeView('services')->set('head_title', $title)
                                           ->set('scripts', ['Public/JavaScript/services-app.js'])
                                           ->set('js_constants', ['BASE_URL' => '\''.WEBROOT.'\'']);

        return $view;
    }
}