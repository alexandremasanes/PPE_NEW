<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 09/07/2016
 * Time: 16:28
 */

/**
 * @Controller
 */
final class AboutController extends AppController{
    /**
     * @Action
     * @Route(pattern="a-propos/", methods={"GET"})
     */
    public function showIndex(): View{
        $title='À propos';
        return $this->makeView('a-propos')->set('head_title',$title);
    }
}