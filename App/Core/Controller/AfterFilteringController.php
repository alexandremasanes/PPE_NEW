<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 15/01/2017
 * Time: 14:08
 */


interface AfterFilteringController{
    public function afterFilter(): void;
}