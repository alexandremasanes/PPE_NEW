<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 03/02/2017
 * Time: 16:21
 */

require_once '../../../autoload.php';
require_once '../autoload.php';
use Doctrine\ORM\Tools\Console\ConsoleRunner;

// replace with file to your own project bootstrap


// replace with mechanism to retrieve EntityManager in your app

define('DOCUMENT_ROOT',__DIR__.'/../../../../');
return ConsoleRunner::createHelperSet(Configuration::getInstance()->getEntityManager());