<?php

declare(strict_types = 1);
assert_options(ASSERT_BAIL);
define('PROD', 0);
if(PROD) error_reporting(0);
define('HTTP_PORT', 80);
define('MYSQL_PORT', 81);
define('HTTP_HOST', $_SERVER['SERVER_NAME']);
define('MYSQL_HOST', 'localhost');
define('ROOT', __DIR__.'/..');
define('DOCUMENT_ROOT', ROOT);
define('WEBROOT', str_replace('main.php', '', $_SERVER['SCRIPT_NAME']));
define('STORAGE_ROOT', DOCUMENT_ROOT.'/Storage/');
define('TEMPLATE_ROOT', DOCUMENT_ROOT.'/App/Template/');
define('TEMPLATE_EXT','.html.php');
define('ERROR_TEMPLATE', 'erreur');
define('URL', 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']); 
define('HTTP_REFERER', $_SERVER['HTTP_REFERER'] ?? WEBROOT);
define('IP_CLIENT', $_SERVER["REMOTE_ADDR"]);
define('REQUEST_METHOD', $_SERVER['REQUEST_METHOD']);
define('KB', 1024);
define('MB', KB*KB);
define('URL_PERCENT','%25');
define('URL_UNDERSCORE','%5F');
//$PDO_OPTIONS[\PDO::ATTR_ERRMODE] = \PDO::ERRMODE_EXCEPTION;